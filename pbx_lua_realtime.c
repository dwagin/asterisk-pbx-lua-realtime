#include "asterisk/app.h"

#include <signal.h>

enum option_flags {
	OPTION_PATTERNS_DISABLED = (1 << 0),
};

AST_APP_OPTIONS(switch_options,
{
AST_APP_OPTION('p', OPTION_PATTERNS_DISABLED),
});

struct cache_entry_key {
	const char *context;
	const char *exten;
	const char *data;
};

struct cache_entry {
	struct timeval when;
	struct ast_variable *var;
	char *context;
	char *exten;
	char data[0];
};

#ifdef LOW_MEMORY
#define CACHE_BUCKETS 17
#else
#define CACHE_BUCKETS 283
#endif

struct ao2_container *cache;
pthread_t cache_cleanup_thread = 0;

static int cache_hash(const void *obj, const int flags)
{
	const struct cache_entry *ce;
	const struct cache_entry_key *key;

	switch (flags & OBJ_SEARCH_MASK) {
	case OBJ_SEARCH_OBJECT:
		ce = obj;
		return ast_str_hash_add(ce->context, ast_str_hash_add(ce->exten, ast_str_hash(ce->data)));
	case OBJ_SEARCH_KEY:
		key = obj;
		return ast_str_hash_add(key->context, ast_str_hash_add(key->exten, ast_str_hash(key->data)));
	default:
		ast_assert(0);
		return 0;
	}
}

static int cache_cmp(void *obj, void *arg, int flags)
{
	const struct cache_entry *ce = obj, *right;
	const struct cache_entry_key *key;

	switch (flags & OBJ_SEARCH_MASK) {
	case OBJ_SEARCH_OBJECT:
		right = arg;
		return strcmp(ce->context, right->context) ? 0 :
		       strcmp(ce->exten, right->exten) ? 0 :
		       strcmp(ce->data, right->data) ? 0 :
		       CMP_MATCH;
	case OBJ_SEARCH_KEY:
		key = arg;
		return strcmp(ce->context, key->context) ? 0 :
		       strcmp(ce->exten, key->exten) ? 0 :
		       strcmp(ce->data, key->data) ? 0 :
		       CMP_MATCH;
	default:
		/* Not supported by container. */
		ast_assert(0);
		return 0;
	}
}

static void free_cache_entry(void *obj)
{
	struct cache_entry *ce = obj;
	ast_variables_destroy(ce->var);
}

static int cache_cleanup_fn(void *obj, void *arg, int flags)
{
	struct cache_entry *ce = obj;
	struct timeval *now = arg;
	return ast_tvdiff_ms(*now, ce->when) >= 2000 ? CMP_MATCH : 0;
}

static void *cache_cleanup(void *unused)
{
	struct timespec forever = {999999999, 0}, one_second = {1, 0};
	struct timeval now;

	for (;;) {
		pthread_testcancel();
		if (ao2_container_count(cache) == 0) {
			nanosleep(&forever, NULL);
		}
		pthread_testcancel();
		now = ast_tvnow();
		ao2_callback(cache, OBJ_MULTIPLE | OBJ_UNLINK | OBJ_NODATA, cache_cleanup_fn, &now);
		pthread_testcancel();
		nanosleep(&one_second, NULL);
	}

	return NULL;
}

static int extension_length_comparator(struct ast_category *p, struct ast_category *q)
{
	const char *extenp = S_OR(ast_variable_find(p, "exten"), "");
	const char *extenq = S_OR(ast_variable_find(q, "exten"), "");

	return strlen(extenp) - strlen(extenq);
}

static struct ast_variable *dup_vars(struct ast_variable *v)
{
	struct ast_variable *new, *list = NULL;
	for (; v; v = v->next) {
		if (!(new = ast_variable_new(v->name, v->value, v->file))) {
			ast_variables_destroy(list);
			return NULL;
		}
		/* Reversed list in cache, but when we duplicate out of the cache,
		 * it's back to correct order. */
		new->next = list;
		list = new;
	}
	return list;
}

/*
 * Realtime looks up extensions in the supplied realtime table.
 *
 * [data][@table][/options]
 *
 * If the realtimetable is omitted it is assumed to be "extensions".
 *
 * The realtime table should have entries for context,exten,data,next_context,next_exten
 */
static struct ast_variable *
realtime_common(const char *table, const char *context, const char *exten, const char *data, int mode,
                struct ast_flags flags)
{
	struct ast_variable *var;
	char *exten_match;
	char exten_pattern[AST_MAX_EXTENSION + 20] = "";

	switch (mode) {
	case E_MATCHMORE:
		exten_match = "exten LIKE";
		snprintf(exten_pattern, sizeof(exten_pattern), "%s_%%", exten);
		break;
	case E_CANMATCH:
		exten_match = "exten LIKE";
		snprintf(exten_pattern, sizeof(exten_pattern), "%s%%", exten);
		break;
	case E_MATCH:
	default:
		exten_match = "exten";
		ast_copy_string(exten_pattern, exten, sizeof(exten_pattern));
	}
	var = ast_load_realtime(table, "data", data, exten_match, exten_pattern, "context", context, SENTINEL);
	if (!var && !ast_test_flag(&flags, OPTION_PATTERNS_DISABLED)) {
		struct ast_config *cfg;
		int match;

		cfg = ast_load_realtime_multientry(
		    table, "data", data, "exten LIKE", "\\_%", "context", context, SENTINEL);
		if (cfg) {
			char *cat = NULL;

			/* Sort so that longer patterns are checked first */
			ast_config_sort_categories(cfg, 1, extension_length_comparator);

			while ((cat = ast_category_browse(cfg, cat))) {
				const char *realtime_exten = ast_variable_retrieve(cfg, cat, "exten");

				if (mode == E_MATCH) {
					match = ast_extension_match(realtime_exten, exten);
				} else {
					match = ast_extension_close(realtime_exten, exten, mode);
				}
				if (match) {
					var = ast_category_detach_variables(ast_category_get(cfg, cat, NULL));
					break;
				}
			}
			ast_config_destroy(cfg);
		}
	}

	return var;
}

static struct ast_variable *
realtime_find_extension(const char *context, const char *exten, int priority, const char *data, int mode)
{
	char *parse, *table, *options;
	struct ast_variable *var = NULL;
	struct ast_flags flags = {0,};
	struct cache_entry *ce;

	/*
	 * Prefix is stripped off in the parent engine.
	 * The remaining string is: [data][@table][/options]
	 */
	parse = ast_strdupa(data);
	options = strchr(parse, '/');
	if (options)
		*options++ = '\0';
	table = strchr(parse, '@');
	if (table) {
		*table++ = '\0';
	}
	data = parse;
	table = S_OR(table, "extensions");
	if (!ast_strlen_zero(options)) {
		ast_app_parse_options(switch_options, &flags, NULL, options);
	}

	/* Search only matches */
	if (mode == E_MATCH) {
		struct cache_entry_key key = {
		    .context = context,
		    .exten = exten,
		    .data = data,
		};
		if ((ce = ao2_find(cache, &key, OBJ_SEARCH_KEY))) {
			var = dup_vars(ce->var);
			ao2_cleanup(ce);
			return var;
		}
	}

	var = realtime_common(table, context, exten, data, mode, flags);

	/* Cache only matches */
	if (mode == E_MATCH) {
		struct ast_variable *new;
		size_t context_size;
		size_t exten_size;
		size_t data_size;

		if (!(new = dup_vars(var))) {
			return var;
		}
		context_size = strlen(context) + 1;
		exten_size = strlen(exten) + 1;
		data_size = strlen(data) + 1;
		if (!(ce = ao2_alloc(sizeof(*ce) + context_size + exten_size + data_size, free_cache_entry))) {
			ast_variables_destroy(new);
			return var;
		}
		ast_copy_string(ce->data, data, data_size);
		ce->exten = ce->data + data_size;
		ast_copy_string(ce->exten, exten, exten_size);
		ce->context = ce->exten + exten_size;
		ast_copy_string(ce->context, context, context_size);
		ce->var = new;
		ce->when = ast_tvnow();
		ao2_link(cache, ce);
		pthread_kill(cache_cleanup_thread, SIGURG);
		ao2_cleanup(ce);
	}

	return var;
}

static int unload_module_extra(void)
{
	pthread_cancel(cache_cleanup_thread);
	pthread_kill(cache_cleanup_thread, SIGURG);
	pthread_join(cache_cleanup_thread, NULL);

	return 0;
}

static int load_module_extra(void)
{
	cache = ao2_container_alloc_hash(AO2_ALLOC_OPT_LOCK_MUTEX, 0, CACHE_BUCKETS, cache_hash, NULL, cache_cmp);
	if (!cache) {
		return AST_MODULE_LOAD_FAILURE;
	}

	if (ast_pthread_create(&cache_cleanup_thread, NULL, cache_cleanup, NULL)) {
		return AST_MODULE_LOAD_FAILURE;
	}

	return AST_MODULE_LOAD_SUCCESS;
}
